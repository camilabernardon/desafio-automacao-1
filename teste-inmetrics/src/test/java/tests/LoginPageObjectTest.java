package tests;

import br.com.inmetrics.teste.support.Web;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import static org.junit.Assert.*;

public class LoginPageObjectTest {

    private WebDriver navegador;

    @Before
    public void setUp(){
        navegador = Web.createChrome();
    }

    @Test
    public void testCreateNewUserSuccess(){
        String view = new LoginPage(navegador)
                .clickCadastraseLink()
                .typeUsername("Maria" +
                        "Joaquina")
                .typePassword("123456")
                .typeConfirmPassword("123456")
                .clickCadastraseButton()
                .viewLoginPage();
        assertEquals("Login", view);
    }

    @Test
    public void testRunLoginSuccess(){
        String viewRegistrosPage = new LoginPage(navegador)
                                .typeUsername("MariaJoaquina")
                                .typePassword("123456")
                                .clickLoginButton()
                                .viewRegistrosPage();
        assertEquals("NOVO FUNCIONÁRIO", viewRegistrosPage);
    }

    @Test
    public void testCreateNewEmployeeSuccess(){
        String textAlert = new LoginPage(navegador)
                .typeUsername("MariaJoaquina")
                .typePassword("123456")
                .clickLoginButton()
                .clickNewEmployeeLink()
                .typeName("New User")
                .typeCPF("01618656236")
                .chooseSexo("Indiferente")
                .typeDateOfHire("20092020")
                .typePosition("QA")
                .typeWage("20000,00")
                .clickTypeOfContract("clt")
                .clickEnviarButtonMsg()
                .capturarTextAlert();
        assertEquals("SUCESSO! Usuário cadastrado com sucesso\n" + "×", textAlert);

    }

    @Test
    public void testEditEmplooyeSuccess(){
        String textAlert = new LoginPage(navegador)
                .typeUsername("MariaJoaquina")
                .typePassword("123456")
                .clickLoginButton()
                .clickNewEmployeeLink()
                .typeName("New User")
                .typeCPF("01618656236")
                .chooseSexo("Indiferente")
                .typeDateOfHire("20092020")
                .typePosition("QA")
                .typeWage("20000,00")
                .clickTypeOfContract("clt")
                .clickEnviarButton()
                .clickFuncionariosLink()
                //.clickSearchEmployee();
                .searchEmployee("New User")
                .clickEditButton()
                .typeName("New User Update")
                .clickEnviarButtonMsg()
                .capturarTextAlert();
        assertEquals("SUCESSO! Informações atualizadas com sucesso\n" + "×", textAlert);
    }

    @Test
    public void testDeleteEmplooyeSuccess(){
        String textAlert = new LoginPage(navegador)
                .typeUsername("MariaJoaquina")
                .typePassword("123456")
                .clickLoginButton()
                .clickNewEmployeeLink()
                .typeName("New User")
                .typeCPF("01618656236")
                .chooseSexo("Indiferente")
                .typeDateOfHire("20092020")
                .typePosition("QA")
                .typeWage("20000,00")
                .clickTypeOfContract("clt")
                .clickEnviarButton()
                .clickFuncionariosLink()
                //.clickSearchEmployee();
                .searchEmployee("New User")
                .clickDeleteButton()
                .capturarTextAlert();
        assertEquals("SUCESSO! Funcionário removido com sucesso\n" + "×", textAlert);
    }

    @After
    public void tearDown(){
        navegador.quit();
    }
}
