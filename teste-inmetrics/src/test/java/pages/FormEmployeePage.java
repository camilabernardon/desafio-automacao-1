package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class FormEmployeePage {
    private WebDriver navegador;

    public FormEmployeePage(WebDriver navegador) {
        this.navegador = navegador;
    }

    public FormEmployeePage typeName(String name){
        navegador.findElement(By.id("inputNome")).sendKeys(name);
        return this;
    }

    public FormEmployeePage typeCPF(String cpf){
        navegador.findElement(By.id("cpf")).sendKeys(cpf);
        return this;
    }

    public FormEmployeePage chooseSexo(String sexo){
        Select sexoo = new Select(navegador.findElement(By.id("slctSexo")));
        sexoo.selectByVisibleText(sexo);
        return this;
    }

    public FormEmployeePage typeDateOfHire(String date){
        navegador.findElement(By.id("inputAdmissao")).sendKeys(date);
        return this;
    }

    public FormEmployeePage typePosition(String position){
        navegador.findElement(By.id("inputCargo")).sendKeys(position);
        return this;
    }

    public FormEmployeePage typeWage(String wage){
        navegador.findElement(By.id("dinheiro")).sendKeys(wage);
        return this;
    }

    public FormEmployeePage clickTypeOfContract(){
        navegador.findElement(By.id("clt")).click();
        return this;
    }

    public FormEmployeePage clickTypeOfContract(String typeContract){
        navegador.findElement(By.id(typeContract)).click();
        return this;
    }

    public BasePage clickEnviarButtonMsg(){
        navegador.findElement(By.cssSelector(".cadastrar-form-btn")).click();
        return new BasePage(navegador);
    }

    public RegistrosPage clickEnviarButton(){
        navegador.findElement(By.cssSelector(".cadastrar-form-btn")).click();
        return new RegistrosPage(navegador);
    }





}
