package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Random;

public class CadastrasePage {

    private WebDriver navegador;

    public CadastrasePage(WebDriver navegador){
        this.navegador = navegador;
    }

    public CadastrasePage typeUsername (String username){
        Random random = new Random();
        navegador.findElement(By.name("username")).sendKeys(username + random.nextInt(10000));
        return this;
    }

    public CadastrasePage typePassword (String password){
        navegador.findElement(By.name("pass")).sendKeys(password);
        return this;
    }

    public CadastrasePage typeConfirmPassword (String confirmpass){
        navegador.findElement(By.name("confirmpass")).sendKeys(confirmpass);
        return this;
    }

    public LoginPage clickCadastraseButton(){
        navegador.findElement(By.cssSelector(".login100-form-btn")).click();
        return new LoginPage(navegador);
    }

    public LoginPage createNewUser(String username, String pass, String confirmpass){
        Random random = new Random();
        navegador.findElement(By.name("username")).sendKeys("username" + random.nextInt(10000));
        navegador.findElement(By.name("pass")).sendKeys("pass");
        navegador.findElement(By.name("confirmpass")).sendKeys("confirmpass");
        navegador.findElement(By.cssSelector(".login100-form-btn")).click();
        return new LoginPage(navegador);
    }
}
