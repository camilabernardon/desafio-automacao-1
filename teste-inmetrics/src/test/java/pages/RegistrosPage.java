package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class RegistrosPage {
    private WebDriver navegador;

    public RegistrosPage(WebDriver navegador) {
        this.navegador = navegador;
    }

    public FormEmployeePage clickNewEmployeeLink(){
        navegador.findElement(By.cssSelector(".nav-item:nth-child(2) > .nav-link")).click();
        return new FormEmployeePage(navegador);
    }

    public String viewRegistrosPage()
    {
        return navegador.findElement(By.cssSelector(".nav-item:nth-child(2) > .nav-link")).getText();
    }

    public RegistrosPage clickFuncionariosLink(){
        navegador.findElement(By.cssSelector(".nav-item:nth-child(1) > .nav-link")).click();
        return this;
    }

    public RegistrosPage clickSearchEmployee (){
        navegador.findElement(By.xpath("//*[@id=\"tabela_filter\"]/label/input")).click();
        // navegador.findElement(By.id("tabela_filter")).findElement(By.cssSelector(".odd:nth-child(1) .btn-warning")).click();
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return this;
    }

    public RegistrosPage searchEmployee (String employee){
        navegador.findElement(By.xpath("//*[@id=\"tabela_filter\"]/label/input")).click();
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        navegador.findElement(By.xpath("//*[@id=\"tabela_filter\"]/label/input")).sendKeys(employee);
        //navegador.findElement(By.id("tabela_filter")).findElement(By.cssSelector(".odd:nth-child(1) .btn-warning")).sendKeys(employee);
        return this;
    }

    public FormEmployeePage clickEditButton(){
        navegador.findElement(By.cssSelector(".odd:nth-child(1) .btn-warning")).click();
        return new FormEmployeePage(navegador);
    }

    public BasePage clickDeleteButton(){
        navegador.findElement(By.id("delete-btn")).click();
        return new BasePage(navegador);
    }
}
