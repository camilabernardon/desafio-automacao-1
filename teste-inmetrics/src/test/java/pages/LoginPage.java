package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Random;

public class LoginPage {

    private WebDriver navegador;

    public LoginPage(WebDriver navegador) {
        this.navegador = navegador;
    }

    //public LoginPage viewLoginPage(String view){
      //  String viewLoginPage = navegador.findElement(By.cssSelector("body > div > div.container-login100 > div > form > span")).getText();
        //return (viewLoginPage);

    //}

    public CadastrasePage clickCadastraseLink(){
        navegador.findElement(By.cssSelector("body > div > div.container-login100 > div > form > div.w-full.text-center.p-t-30 > a")).click();
        return new CadastrasePage(navegador);
    }


    public LoginPage typeUsername (String username){
        navegador.findElement(By.name("username")).sendKeys(username);
        return this;
    }

    public LoginPage typePassword (String pass){
        navegador.findElement(By.name("pass")).sendKeys(pass);
        return this;
    }

    public RegistrosPage clickLoginButton(){
        navegador.findElement(By.cssSelector(".login100-form-btn")).click();
        return new RegistrosPage(navegador);
    }

    public String viewLoginPage (){
        return navegador.findElement(By.cssSelector("body > div > div.container-login100 > div > form > span")).getText();
    }
}