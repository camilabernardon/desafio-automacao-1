package br.com.inmetrics.teste.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class Web {
    public static WebDriver createChrome (){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\camil\\Drivers\\chromedriver.exe");
        WebDriver navegador = new ChromeDriver();
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        navegador.get("http://www.inmrobo.tk/accounts/login/");
        navegador.manage().window().maximize();
        return navegador;
    }
}
